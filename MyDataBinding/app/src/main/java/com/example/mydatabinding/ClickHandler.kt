package com.example.mydatabinding

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.View
import android.widget.TextView

class ClickHandler {

    fun onUserNameClicked(view: View, user: User){
        val tvName = view as TextView
        tvName.setBackgroundColor(Color.YELLOW)
        tvName.setTextColor(Color.RED)
        tvName.text = user.name.toUpperCase()
    }
    fun onButtonSecondActivityClicked(context: Context){
        context.startActivity(Intent(context, SecondActivity::class.java))
    }
}